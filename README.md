# Collect and Set Useful Plugins

Just pull and use quickly...

## Ingredients

- Bootstrap
- Jquery
- Autoprefixer
- Swiper
- AOS

## Module Bundler

[Parcel](https://parceljs.org)

## Goal

To Save Time For Frontend Projects

## Usage

```npm
npm install
```
